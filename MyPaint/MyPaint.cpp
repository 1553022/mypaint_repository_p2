﻿// MyPaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyPaint.h"



int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_MYPAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MYPAINT);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassExW(&wcex);

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndNonameProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 4;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDI_MYPAINT);
	wcex.lpszClassName = L"MDI NONAME";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassExW(&wcex);

	return 1;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hFrameWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hFrameWnd)
	{
		return FALSE;
	}

	ShowWindow(hFrameWnd, nCmdShow);
	UpdateWindow(hFrameWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
int nNoname = 0;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND			hwndChild;
	MDICREATESTRUCT mdiCreate;
	switch (message)
	{
	case WM_SIZE:
	{
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 28, w, h - 28, TRUE);
		return 0;
	}
	break;
	case WM_CREATE:
	{
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
		ccs.idFirstChild = 50001;
		hwndMDIClient = CreateWindow(L"MDICLIENT",
			(LPCTSTR)NULL,
			WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL,
			0, 0, 0, 0,
			hWnd,
			(HMENU)NULL,
			hInst,
			(LPVOID)&ccs);
		ShowWindow(hwndMDIClient, SW_SHOW);

		CreateToolbar(hWnd);
		return 0;
	}
	break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_NEW:
		{
			mdiCreate.szClass = L"MDI NONAME";
			nNoname = nNoname + 1;
			WCHAR input[MAX_LOADSTRING];
			swprintf_s(input, L"Noname%d.drw", nNoname);
			mdiCreate.szTitle = input;
			mdiCreate.hOwner = hInst;
			mdiCreate.x = CW_USEDEFAULT;
			mdiCreate.y = CW_USEDEFAULT;
			mdiCreate.cx = CW_USEDEFAULT;
			mdiCreate.cy = CW_USEDEFAULT;
			mdiCreate.style = 0;

			mdiCreate.lParam = NULL;
			hwndChild = (HWND)SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPCREATESTRUCT)&mdiCreate);
			return 0;
		}



		case ID_WINDOW_CASCADE:
			SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
			return 0;
		case ID_WINDOW_CLOSEALL:
			EnumChildWindows(hwndMDIClient, (WNDENUMPROC)CloseProc, 0L);
			return 0;
		case ID_WINDOW_TIDE:
			SendMessage(hwndMDIClient, WM_MDITILE, 0, 0);
			return 0;
		case ID_FILE_OPEN:
			OpenBox(hWnd);
			return 0;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			return 0;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			return 0;
		default:
			hwndChild = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			if (IsWindow(hwndChild))
				SendMessage(hwndChild, WM_COMMAND, wParam, lParam);
			break;
		}
	}
	break;
	case WM_QUERYENDSESSION:
	case WM_CLOSE:                      // Attempt to close all children

		SendMessage(hWnd, WM_COMMAND, ID_WINDOW_CLOSEALL, 0);

		if (NULL != GetWindow(hwndMDIClient, GW_CHILD))
			return 0;

		break;   // i.e., call DefFrameProc 
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

int x1, x2, y1, y2;
LRESULT CALLBACK WndNonameProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	static win *w = 0; //
	switch (message)
	{
	case WM_CREATE:
		w = (win*)VirtualAlloc(NULL, sizeof(win), MEM_COMMIT, PAGE_READWRITE);
		if (!w)
		{
			MessageBox(hWnd, L"Unable to retrieve pointer.", 0, 0);
			SendMessage(hWnd, WM_CLOSE, 0, 0);
		}
		SetWindowLong(hWnd, 0, (LONG)w); // lưu con trỏ struct vào 4 byte mở rộng của window
		w->tool = LINE;
		w->nGraph = 0;
		return 0;
	case WM_COMMAND:
		w = (win*)GetWindowLong(hWnd, 0);
		switch (LOWORD(wParam))
		{
		case ID_FILE_SAVE:
			//SaveBox(hWnd);
			MessageBox(hWnd, w->allData(), L"sFD", MB_OK);
			break;
		case ID_DRAW_COLOR:
			w->c_cur = displayColorDlg(hWnd);
			break;
		case ID_DRAW_FONT:
			w->f_cur = displayFontDlg(hWnd);
			break;
		case ID_DRAW_ELLIPSE:
			w->tool = ELLIPSE;
			break;
		case ID_DRAW_LINE:
			w->tool = LINE;
			break;
		case ID_DRAW_RECTANGLE:
			w->tool = RECTANGLE;
			break;
		case ID_DRAW_TEXT:
			w->tool = TEXT_P;
			break;
		}
		return 0;
	case WM_MDIACTIVATE:
		w = (win*)GetWindowLong(hWnd, 0);
		return 0;
	case WM_LBUTTONUP:
		w = (win*)GetWindowLong(hWnd, 0);
		w->nGraph++;
		return 0;
	case WM_LBUTTONDOWN:
		w = (win*)GetWindowLong(hWnd, 0);
		OnLButtonDown(lParam, x1, y1, x2, y2);
		if (w->tool == TEXT_P)
		{
			w->g[w->nGraph] = new text(x1, y1, x1, y1);
			w->g[w->nGraph]->Draw(hWnd, wParam, lParam, x1, y1, x1, x1, w->c_cur, w->f_cur);
			w->nGraph++;
		}
		return 0;
	case WM_MOUSEMOVE:
		w = (win*)GetWindowLong(hWnd, 0);
		switch (w->tool) {
		case LINE: {
			w->g[w->nGraph] = new line(x1, y1, x2, y2);
			w->g[w->nGraph]->Draw(hWnd, wParam, lParam, x1, y1, x2, y2, w->c_cur, w->f_cur);
			break;
		}
		case RECTANGLE: {
			w->g[w->nGraph] = new rectangle(x1, y1, x2, y2);
			w->g[w->nGraph]->Draw(hWnd, wParam, lParam, x1, y1, x2, y2, w->c_cur, w->f_cur);
			break;
		}
		case ELLIPSE: {
			w->g[w->nGraph] = new ellipse(x1, y1, x2, y2);
			w->g[w->nGraph]->Draw(hWnd, wParam, lParam, x1, y1, x2, y2, w->c_cur, w->f_cur);
			break;
		}
		}

		return 0;
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		w = (win*)GetWindowLong(hWnd, 0);
		if (w->nGraph > 0) {
			for (int i = 0; i < w->nGraph; i++)
			{
				w->g[i]->paint(hWnd);
			}
		}
		EndPaint(hWnd, &ps);
		return 0;
	}
	case WM_DESTROY:
		w = (win*)GetWindowLong(hWnd, 0);

		VirtualFree(w, sizeof(win), MEM_COMMIT);
		return 0;
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}


void onNonameWindow(HWND hwnd)
{
	MDICREATESTRUCT mdiCreate;
	mdiCreate.szClass = L"MDI NONAME";
	nNoname = nNoname + 1;
	WCHAR input[MAX_LOADSTRING];
	swprintf_s(input, L"Noname%d.drw", nNoname);
	mdiCreate.szTitle = input;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;

	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPCREATESTRUCT)&mdiCreate);
}

LRESULT CALLBACK CloseProc(HWND hwndChild, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hwndChild, 0L);
	return 1;
}
