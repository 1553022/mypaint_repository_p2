#pragma once

#include "resource.h"


#include <commdlg.h>
#include <CommCtrl.h>
#include<Windows.h>
#include<stdio.h>
//#include <string>
//#include <fstream>
//#include <iostream>
//using std::string;

#define MAX_GRPAH 100
#define MAX_LOADSTRING 100


#define LINE 1000000
#define RECTANGLE  1000001
#define ELLIPSE  1000002
#define TEXT_P 1000003


// Global Variables:
HWND hwndMDIClient = NULL;
HWND hFrameWnd = NULL;
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

												// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndNonameProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void onNonameWindow(HWND hwnd);
LRESULT CALLBACK CloseProc(HWND hwndChild, LPARAM lParam);
COLORREF displayColorDlg(HWND hwnd);
LPLOGFONTW displayFontDlg(HWND hwnd);
HWND CreateToolbar(HWND hWndParent);
HWND hWndToolbar = NULL;

void OpenBox(HWND hWnd)
{
	OPENFILENAME ofn;
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("Draw file\0 * .drw\0");
	szFile[0] = '\0';

	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn))
	{
		MessageBox(hWnd, ofn.lpstrFile, TEXT("Your open link documment"), MB_OK);
	}
}

void SaveBox(HWND hWnd)
{
	OPENFILENAME save;
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("Draw file\0 * .drw\0");
	szFile[0] = '\0';

	ZeroMemory(&save, sizeof(OPENFILENAME));
	save.lStructSize = sizeof(OPENFILENAME);
	save.hwndOwner = hWnd;
	save.lpstrFilter = szFilter;
	save.nFilterIndex = 1;
	save.lpstrFile = szFile;
	save.nMaxFile = sizeof(szFile);
	save.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetSaveFileName(&save))
	{
		MessageBox(hWnd, save.lpstrFile, TEXT("Your save link documment"), MB_OK);
	}
}

COLORREF displayColorDlg(HWND hwnd)
{
	CHOOSECOLOR cc;                 // common dialog box structure 
	static COLORREF acrCustClr[16]; // array of custom colors 
									//HWND hwnd;                      // owner window
	HBRUSH hbrush;                  // brush handle
	static DWORD rgbCurrent;        // initial color selection

									// Initialize CHOOSECOLOR 
	ZeroMemory(&cc, sizeof(cc));
	cc.lStructSize = sizeof(cc);
	cc.hwndOwner = hwnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cc) == TRUE)
	{
		hbrush = CreateSolidBrush(cc.rgbResult);
		rgbCurrent = cc.rgbResult;
	}
	return cc.rgbResult;
}
LPLOGFONTW displayFontDlg(HWND hwnd)
{
	//HWND hwnd;                // owner window
	HDC hdc;                  // display device context of owner window
	hdc = GetDC(hwnd);
	CHOOSEFONT cf;            // common dialog box structure
	static LOGFONT lf;        // logical font structure
	static DWORD rgbCurrent;  // current text color
	HFONT hfont, hfontPrev;
	DWORD rgbPrev;

	// Initialize CHOOSEFONT
	ZeroMemory(&cf, sizeof(cf));
	cf.lStructSize = sizeof(cf);
	cf.hwndOwner = hwnd;
	cf.lpLogFont = &lf;
	cf.rgbColors = rgbCurrent;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;

	if (ChooseFont(&cf) == TRUE)
	{
		hfont = CreateFontIndirect(cf.lpLogFont);
		hfontPrev = (HFONT)SelectObject(hdc, hfont);
		rgbCurrent = cf.rgbColors;
		rgbPrev = SetTextColor(hdc, rgbCurrent);
	}
	return cf.lpLogFont;
}

HIMAGELIST hImageList = NULL;

HWND CreateToolbar(HWND hWndParent)
{
	// Declare and initialize local constants.
	const int ImageListID = 0;
	const int numButtons = 8;
	const int bitmapSize = 16;

	const DWORD buttonStyles = BTNS_AUTOSIZE;

	// Create the toolbar.
	HWND hWndToolbar = CreateWindow(TOOLBARCLASSNAME, NULL,
		WS_CHILD | TBSTYLE_WRAPABLE,
		0, 0, 0, 0,
		hWndParent, NULL, hInst, NULL);

	if (hWndToolbar == NULL)
		return NULL;


	hImageList = ImageList_LoadImage(hInst, MAKEINTRESOURCE(IDB_BITMAP1), 16, 10, CLR_DEFAULT,
		IMAGE_BITMAP, LR_CREATEDIBSECTION | LR_DEFAULTSIZE | LR_SHARED | LR_LOADTRANSPARENT);
	HIMAGELIST hOld = (HIMAGELIST)SendMessage(hWndToolbar, TB_SETIMAGELIST, 0, (LPARAM)hImageList);


	// Sending this ensures iString(s) of TBBUTTON structs become tooltips rather than button text
	SendMessage(hWndToolbar, TB_SETMAXTEXTROWS, 0, 0);

	DWORD dwCountImages = SendMessage(hWndToolbar, TB_LOADIMAGES, (WPARAM)IDB_STD_SMALL_COLOR, (LPARAM)HINST_COMMCTRL);



	// Initialize button info.
	// IDM_NEW, IDM_OPEN, and IDM_SAVE are application-defined command constants.

	TBBUTTON tbButtons[numButtons] =
	{
		{ dwCountImages + STD_FILENEW,	ID_FILE_NEW,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ dwCountImages + STD_FILEOPEN, ID_FILE_OPEN,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ dwCountImages + STD_FILESAVE, ID_FILE_SAVE,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ 0,							0,					TBSTATE_ENABLED, TBSTYLE_SEP,{ 0 }, 0 },
		{ 0,							ID_DRAW_LINE,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ 1,							ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ 2,							ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 },
		{ 3,							ID_DRAW_TEXT,		TBSTATE_ENABLED, buttonStyles,{ 0 }, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons


	// Add buttons.
	SendMessage(hWndToolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
	SendMessage(hWndToolbar, TB_ADDBUTTONS, (WPARAM)numButtons, (LPARAM)&tbButtons);

	// Resize the toolbar, and then show it.
	SendMessage(hWndToolbar, TB_AUTOSIZE, 0, 0);
	ShowWindow(hWndToolbar, TRUE);

	return hWndToolbar;
}

struct Graph
{
	LPLOGFONTW f;
	COLORREF c;
	int gx1, gx2, gy1, gy2;
	int type;

	Graph(int x1, int x2, int y1, int y2) {
		gx1 = x1;
		gx2 = x2;
		gy1 = y1;
		gy2 = y2;
	}
	virtual void Draw(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2, COLORREF c_cur, LPLOGFONTW f_cur) {}
	virtual void paint(HWND hWnd) {}
};

struct win {
	HWND h;
	Graph *g[MAX_GRPAH];
	int nGraph;
	COLORREF c_cur;
	LPLOGFONTW f_cur;
	int tool;

	TCHAR* allData()
	{
		TCHAR buff[MAX_LOADSTRING];
		ZeroMemory(buff, sizeof(buff));
		swprintf_s(buff, L"%d\n", nGraph);
		swprintf_s(buff, L"%d\n", nGraph);
		//swprintf_s(buff, L"R: %i, G : %i, B : %i", GetRValue(c_cur), GetGValue(c_cur), GetBValue(c_cur));
		
		return buff;
	}

};

void OnLButtonDown(LPARAM lParam, int &x1, int &y1, int &x2, int &y2)
{
	x1 = x2 = LOWORD(lParam);
	y1 = y2 = HIWORD(lParam);
}

struct line : public Graph
{
public:
	line(int x1, int x2, int y1, int y2) : Graph(x1, x2, y1, y2) {};
	virtual void Draw(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2, COLORREF c_cur, LPLOGFONTW f_cur)
	{
		if (wParam & MK_LBUTTON) {
			type = LINE;
			c = c_cur;
			HDC dc = GetDC(hWnd);

			HPEN hbrush;
			hbrush = CreatePen(PS_SOLID, 1, c);
			SelectObject(dc, hbrush);

			SetROP2(dc, R2_NOTXORPEN);
			MoveToEx(dc, x1, y1, NULL);
			LineTo(dc, x2, y2);

			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);

			MoveToEx(dc, x1, y1, NULL);
			LineTo(dc, x2, y2);

			SelectObject(dc, hbrush);
			DeleteObject(hbrush);
			ReleaseDC(hWnd, dc);


			gx1 = x1;
			gx2 = x2;
			gy1 = y1;
			gy2 = y2;

		}
	}

	virtual void paint(HWND hWnd) {
		HDC dc = GetDC(hWnd);

		HBRUSH hbrush, hbrushOld;
		hbrush = CreateSolidBrush(c);
		hbrushOld = (HBRUSH)SelectObject(dc, hbrush);

		MoveToEx(dc, gx1, gy1, NULL);
		LineTo(dc, gx2, gy2);

		SelectObject(dc, hbrushOld);
		DeleteObject(hbrush);
		ReleaseDC(hWnd, dc);
	}
};

struct rectangle : public Graph
{
	rectangle(int x1, int x2, int y1, int y2) : Graph(x1, x2, y1, y2) {};

	virtual void Draw(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2, COLORREF c_cur, LPLOGFONTW f_cur)
	{
		if (wParam & MK_LBUTTON) {
			type = RECTANGLE;
			c = c_cur;
			HDC dc = GetDC(hWnd);

			HBRUSH hbrush;
			hbrush = CreateSolidBrush(c);
			SelectObject(dc, hbrush);

			SetROP2(dc, R2_NOTXORPEN);
			MoveToEx(dc, x1, y1, NULL);
			Rectangle(dc, x1, y1, x2, y2);

			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);

			MoveToEx(dc, x1, y1, NULL);
			Rectangle(dc, x1, y1, x2, y2);

			SelectObject(dc, hbrush);
			DeleteObject(hbrush);
			ReleaseDC(hWnd, dc);

			gx1 = x1;
			gx2 = x2;
			gy1 = y1;
			gy2 = y2;
		}
	}

	virtual void paint(HWND hWnd) {
		HDC dc = GetDC(hWnd);

		HBRUSH hbrush, hbrushOld;
		hbrush = CreateSolidBrush(c);
		hbrushOld = (HBRUSH)SelectObject(dc, hbrush);

		Rectangle(dc, gx1, gy1, gx2, gy2);

		SelectObject(dc, hbrushOld);
		DeleteObject(hbrush);
		ReleaseDC(hWnd, dc);
	}
};


struct ellipse : public Graph
{
	ellipse(int x1, int x2, int y1, int y2) : Graph(x1, x2, y1, y2) {};

	virtual void Draw(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2, COLORREF c_cur, LPLOGFONTW f_cur)
	{
		if (wParam & MK_LBUTTON)
		{
			type = ELLIPSE;
			c = c_cur;
			HDC dc = GetDC(hWnd);

			HBRUSH hbrush;
			hbrush = CreateSolidBrush(c);
			SelectObject(dc, hbrush);

			SetROP2(dc, R2_NOTXORPEN);
			MoveToEx(dc, x1, y1, NULL);
			Ellipse(dc, x1, y1, x2, y2);

			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);

			MoveToEx(dc, x1, y1, NULL);
			Ellipse(dc, x1, y1, x2, y2);

			SelectObject(dc, hbrush);
			DeleteObject(hbrush);
			ReleaseDC(hWnd, dc);

			gx1 = x1;
			gx2 = x2;
			gy1 = y1;
			gy2 = y2;
		}
	}

	virtual void paint(HWND hWnd) {
		HDC dc = GetDC(hWnd);

		HBRUSH hbrush, hbrushOld;
		hbrush = CreateSolidBrush(c);
		hbrushOld = (HBRUSH)SelectObject(dc, hbrush);

		Ellipse(dc, gx1, gy1, gx2, gy2);

		SelectObject(dc, hbrushOld);
		DeleteObject(hbrush);
		ReleaseDC(hWnd, dc);
	}
};


int length = 0;
TCHAR szItemName[MAX_LOADSTRING];
INT_PTR CALLBACK inputTextProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		ZeroMemory(szItemName, MAX_LOADSTRING);
		length = 0;
		EndDialog(hwndDlg, (INT_PTR)szItemName);
		return (INT_PTR)FALSE;
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			length = GetDlgItemText(hwndDlg, IDC_EDIT1, szItemName, MAX_LOADSTRING);
			EndDialog(hwndDlg, (INT_PTR)szItemName);
			return (INT_PTR)TRUE;
			break;
		}
	}
	return (INT_PTR)FALSE;
}


struct text : public Graph
{
	TCHAR t[MAX_LOADSTRING];
	int length_t;

	text(int x1, int x2, int y1, int y2) : Graph(x1, x2, y1, y2) {};


	virtual void Draw(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2, COLORREF c_cur, LPLOGFONTW f_cur)
	{
		if (wParam & MK_LBUTTON) {
			type = TEXT_P;
			c = c_cur;
			f = f_cur;
			HDC dc = GetDC(hWnd);

			HFONT hFont;
			hFont = CreateFontIndirect(f);
			SelectObject(dc, hFont);
			SetTextColor(dc, c);

			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, inputTextProc);
			ZeroMemory(t, length);

			_tcscpy_s(t, szItemName);
			length_t = length;
			ZeroMemory(szItemName, length);

			TextOut(dc, x1, y1, t, length_t);

			SelectObject(dc, hFont);
			DeleteObject(hFont);
			ReleaseDC(hWnd, dc);

			gx1 = x1;
			gx2 = x2;
			gy1 = y1;
			gy2 = y2;
		}
	}

	virtual void paint(HWND hWnd) {
		HDC dc = GetDC(hWnd);

		HFONT hFont;
		hFont = CreateFontIndirect(f);
		SelectObject(dc, hFont);
		SetTextColor(dc, c);

		TextOut(dc, gx1, gy1, t, length_t);

		SelectObject(dc, hFont);
		DeleteObject(hFont);
		ReleaseDC(hWnd, dc);
	}
};
